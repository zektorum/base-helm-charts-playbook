# base-helm-charts-playbook
Ansible playbook for deploying some helm charts to Yandex Cloud.

### Running
This command install ingress controller, cert-manager, and ExternalDNS charts.
```bash
ansible-playbook ingress.yaml
```

To install Grafana and Prometheus charts, run this
```bash
ansible-playbook monitoring.yaml
```

